from docutils.nodes import reference
from docutils.utils import unescape
from docutils.parsers.rst.roles import set_classes
import re

re_generic_slug_match = re.compile("<([^>]+)>(.+)")


def bl_rev_role(name, rawtext, text, lineno, inliner, options={}, content=[]):
    if " " in text:
        msg = inliner.reporter.error(
            '"rev" role expected a sha1; '
            '"%s" is invalid.' % text, line=lineno)
        prb = inliner.problematic(rawtext, rawtext, msg)
        return [prb], [msg]

    app = inliner.document.settings.env.app
    node = make_link_node(rawtext, app, text, options)
    return [node], []


def make_link_node(rawtext, app, slug, options):
    # Check for GITEA slug:
    if (m := re_generic_slug_match.match(slug)) is not None:
        repo = m.group(1)
        sha1 = m.group(2)
        ref = "https://projects.blender.org/{:s}/commit/{:s}".format(repo, sha1)
        set_classes(options)
        node = reference(
            rawtext,
            # text to draw
            # shorten sha1 for display
            unescape("{:s}@{:s}".format(repo, sha1)),
            # url to link to
            refuri=ref,
            **options,
        )
        return node

    try:
        base = app.config.bl_rev_site_base
        if not base:
            raise AttributeError
    except AttributeError as err:
        raise ValueError('"bl_rev_site_base" configuration value is not set (%s)' % str(err))
    try:
        prefix_map = app.config.bl_rev_site_prefix_map
        if not prefix_map:
            raise AttributeError
    except AttributeError as err:
        raise ValueError('"bl_rev_site_prefix_map" configuration value is not set (%s)' % str(err))

    prefix = None
    for k, v in prefix_map.items():
        if slug.startswith(k):
            prefix = k
            site_subpage = v
            slug_noprefix = slug[len(k):]
            break

    if prefix is None:
        raise ValueError('"slug": %s did not have a prefix in: %r' % (slug, tuple(prefix_map.keys())))

    ref = base + site_subpage + slug_noprefix

    set_classes(options)
    node = reference(
        rawtext,
        # text to draw
        # shorten sha1 for display
        unescape(slug[:8 + len(prefix)]),
        # url to link to
        refuri=ref,
        **options,
    )
    return node


def setup(app):
    app.add_role('rev', bl_rev_role)
    app.add_config_value('bl_rev_site_base', None, 'env')
    app.add_config_value('bl_rev_site_prefix_map', None, 'env')
    return {'parallel_read_safe': True}
