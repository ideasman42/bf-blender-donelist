#!/bin/bash
BASE_DIR="$( dirname -- "${BASH_SOURCE[0]}"; )";
cd $BASE_DIR/../

../blender/tools/triage/weekly_report.py --username=ideasman42 --hash-length=0 | \
    ./utils/weekly_report_from_blender_to_rst.py

echo ""

git -C ../blender/ log --since="14 days ago"  --committer="Campbell" --pretty=format:"%cD %H %s ...  %an" | grep -v "Campbell Barton"
