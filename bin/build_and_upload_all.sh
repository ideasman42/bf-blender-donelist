#!/usr/bin/env bash

# The directory "above" this directory.
BASE_DIR=$(cd -- "$( dirname -- "${BASH_SOURCE[0]}" )/../" &> /dev/null && pwd)

# print out commands
set -x

# This projects base.
cd $BASE_DIR

make
if [ $? -ne 0 ]; then echo "error -> $?" ; fi

# Done markdown conversion.

make upload

# Markdown conversion.
python ./rst_to_md_batch.py

function git_push_if_needed() {
  local a="main" b="origin/main"
  local aref=$(git rev-parse $a)
  local bref=$(git rev-parse $b)

  # Push unless there is an exact match.
  if [[ $aref == $bref ]]; then
    exit 0
  fi
  git push
}

# Commit this repo.
git ci -a -m "Weekly report"
git_push_if_needed

# Commit the profiles repo.
cd $BASE_DIR/../blender_profile
git ci -a -m "Weekly report"
git_push_if_needed
