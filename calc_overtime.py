#!/usr/bin/env python

"""
This script calculates time in the simple  format defined by: utimeclock, see:
https://codeberg.org/ideasman42/emacs-utimeclock
"""
import sys
import re

from typing import (
    Dict,
    Tuple,
    Optional,
)

hours_worked_filename = sys.argv[-1]


class term_colors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


def parse_date_line(line: str) -> Optional[Tuple[Tuple[int, int, int], Dict[str, str]]]:
    # Expect date:
    m = re.match(r"(\d+)/(\d+)/(\d+)\s+\(([^)]*)\).*", line)
    if not m:
        return None

    # Year, month, day
    date = int(m.group(1)), int(m.group(2)), int(m.group(3))

    var_dict = {"days": "5"}
    var_list = m.group(4).split(",")
    for var_parse in var_list:
        var_parse = var_parse.split("=", 1)
        if len(var_parse) == 1:
            continue
        var_parse[:] = [v.strip() for v in var_parse]
        assert isinstance(var_parse[1], str)
        var_dict[var_parse[0]] = var_parse[1]

    return date, var_dict


def time_as_min(hr_min: str) -> int:
    hr, mn = (int(t) for t in hr_min.partition(":")[0::2])
    if mn >= 60:
        raise Exception("Malformed time, >= 60min found")
    return (hr * 60) + mn


def parse_time_range_as_min(ranges: str) -> int:
    mn = 0
    for span in ranges.split():
        beg, end = span.partition("-")[0::2]
        beg_as_min = time_as_min(beg)
        end_as_min = time_as_min(end)

        val = end_as_min - beg_as_min
        if val < 0:
            val += (12 * 60)
        if val < 0:
            raise Exception("Malformed time, not mapping within 12 hrs")
        mn += val
    return mn


def min_as_time(t: int) -> str:
    a = t // 60
    b = t - (a * 60)
    return "{:02d}:{:02d}".format(a, b)


def min_as_time_or_blank(t: int) -> str:
    if t == 0:
        return "--:--"
    return min_as_time(t)


def text_as_green(text: str) -> str:
    return term_colors.OKBLUE + text + term_colors.ENDC


def text_as_red(text: str) -> str:
    return term_colors.FAIL + text + term_colors.ENDC


def main() -> None:
    line_peek = None
    sick_leave_total_days = 0
    time_total_min = 0
    overtime_total_min = 0
    overtime_total_min_offset = 0
    with open(hours_worked_filename, 'r') as fh:
        line_index = -1
        while line := (line_peek if line_peek is not None else fh.readline()):
            line_peek = None
            line_index += 1

            date_args = parse_date_line(line)
            if date_args is None:
                print("Expected: 'YYYY/MM/DD: (args=vars)' at line {:d}".format(line_index + 1))
                sys.exit(1)
            date, args = date_args
            if "days" not in args:
                print("Expected: 'days' argument at line {:d}".format(line_index + 1))
                sys.exit(1)

            time = []
            while line_time := (line_peek if line_peek is not None else fh.readline()):
                line_peek = None
                line_index += 1

                if not line_time.lstrip().startswith("time:"):
                    line_peek = line_time
                    line_index -= 1
                    break

                try:
                    time_day = parse_time_range_as_min(line_time.partition("time:")[2].strip())
                except Exception as ex:
                    print("Error parsing time: ({:s}) at line {:d}".format(str(ex), line_index + 1))
                    sys.exit(1)

                time.append(time_day)

            time_actual = sum(time)
            time_target = int(args["days"]) * (8 * 60)
            time_overtime = time_actual - time_target

            sick_leave_days = int(args.get("sick_days", 0))
            sick_leave_total_days += sick_leave_days

            overtime_total_min += time_overtime

            overtime_offset = args.get("overtime")
            if overtime_offset is not None:
                overtime_total_min_offset = time_as_min(overtime_offset)
                break

            if time:
                daily_text = "(daily: {:s})".format(" ".join(min_as_time_or_blank(t) for t in time))
            else:
                daily_text = "--"

            if sick_leave_days:
                daily_text = daily_text + ", sick days " + str(sick_leave_days)

            print("Week of {:04d}/{:02d}/{:02d}, days={:d}, worked {:s} of {:s}, [{:s}], {:s} (overtime: {:s})".format(
                date[0],
                date[1],
                date[2],
                int(args["days"]),
                min_as_time(time_actual),
                min_as_time(time_target),
                (text_as_red if time_overtime < 0 else text_as_green)(
                    ("+-"[time_overtime < 0]) + min_as_time(abs(time_overtime))
                ),
                daily_text,
                min_as_time(overtime_total_min),
            ))

            time_total_min += time_actual

    print("Total time: {:s}".format(min_as_time(time_total_min)))
    print("Total overtime is: {:s}".format(min_as_time(overtime_total_min + overtime_total_min_offset)))
    print("Total sick leave is: {:d} days".format(sick_leave_days))


if __name__ == "__main__":
    main()
