;;; (rst-mode).el --- RST Configuration -*- lexical-binding: t -*-


(defconst this-dir (expand-file-name (file-name-concat (file-name-directory load-file-name) "..")))

;; The Blender source code directory.
(defconst this-blender-dir (expand-file-name (file-name-concat this-dir ".." "blender")))

(defconst this-ucalendar-dir
  (expand-file-name (file-name-concat this-dir ".." "emacs" "ucalendar")))

(add-to-list 'load-path this-ucalendar-dir)
(autoload 'ucalendar-report "ucalendar")

(setq-local my-generic-build
            (lambda ()
              (interactive)
              (let
                  ( ;; ensure 'compile-command' is used.
                   (compilation-read-command nil)
                   (compile-command (concat this-dir "/bin/build_and_upload_all.sh")))
                (call-interactively 'compile))))

(setq-local my-mode-line-idle
            (append
             my-mode-line-idle
             (list
              '(:eval
                (let ((time-summary
                       (with-demoted-errors "TimeClock: %S"
                         (utimeclock-from-context-summary))))
                  (when time-summary
                    (list '(:propertize "hours " face font-lock-doc-face) time-summary))))
              '(:eval
                (let ((cal-summary
                       (with-demoted-errors "Calendar: %S"
                         (ucalendar-report))))
                  (when cal-summary
                    (list '(:propertize " calendar" face font-lock-doc-face) cal-summary)))))))

;; Use so (Select, Return) evaluates Blender/RST plugins.
(setq-local my-generic-eval-selection
            (lambda ()
              (interactive)
              (save-restriction
                (let ((table (make-syntax-table))
                      (count 0))
                  (with-syntax-table table
                    (when (region-active-p)
                      (narrow-to-region (region-beginning) (region-end)))

                    (goto-char (point-min))
                    (while (re-search-forward "\\bD\\([[:digit:]#]+\\)\\b" nil t)
                      (setq count (1+ count))
                      (replace-match ":diff:`\\1`" t nil nil))

                    (goto-char (point-min))
                    (while (re-search-forward "\\bP\\([[:digit:]#]+\\)\\b" nil t)
                      (setq count (1+ count))
                      (replace-match ":paste:`\\1`" t nil nil))

                    (goto-char (point-min))
                    (while (re-search-forward "#\\([[:digit:]#]+\\)\\b" nil t)
                      (setq count (1+ count))
                      (replace-match ":task:`\\1`" t nil nil))

                    (goto-char (point-min))
                    (while (re-search-forward "!\\([[:digit:]#]+\\)\\b" nil t)
                      (setq count (1+ count))
                      (replace-match ":pr:`\\1`" t nil nil))

                    ;; Blender.
                    (goto-char (point-min))
                    (while (re-search-forward "\\brB\\([[:digit:]a-zA-F]+\\)\\b" nil t)
                      (setq count (1+ count))
                      (replace-match ":rev:`B\\1`" t nil nil))

                    ;; Blender library.
                    (goto-char (point-min))
                    (while (re-search-forward "\\brBL\\([[:digit:]a-zA-F]+\\)\\b" nil t)
                      (setq count (1+ count))
                      (replace-match ":rev:`BL\\1`" t nil nil))

                    ;; Blender development tools.
                    (goto-char (point-min))
                    (while (re-search-forward "\\brBDT\\([[:digit:]a-zA-F]+\\)\\b" nil t)
                      (setq count (1+ count))
                      (replace-match ":rev:`BDT\\1`" t nil nil))

                    ;; Blender developer handbook (manual).
                    (goto-char (point-min))
                    (while (re-search-forward "\\brBDM\\([[:digit:]a-zA-F]+\\)\\b" nil t)
                      (setq count (1+ count))
                      (replace-match ":rev:`BDM\\1`" t nil nil))
                    (while (re-search-forward "!BDM\\([[:digit:]#]+\\)\\b" nil t)
                      (setq count (1+ count))
                      (replace-match ":pr:`BDM\\1`" t nil nil))

                    ;; Blender manual.
                    (goto-char (point-min))
                    (while (re-search-forward "\\brBM\\([[:digit:]a-zA-F]+\\)\\b" nil t)
                      (setq count (1+ count))
                      (replace-match ":rev:`BM\\1`" t nil nil))
                    (goto-char (point-min))
                    (while (re-search-forward "!BM\\([[:digit:]#]+\\)\\b" nil t)
                      (setq count (1+ count))
                      (replace-match ":pr:`BM\\1`" t nil nil))

                    ;; Finally move the end #123 to be beginning and format is as a PR.
                    (goto-char (point-min))
                    (while (re-search-forward ":task:`[[:digit:]]+`" nil t)
                      (let ((beg (match-beginning 0))
                            (end (match-end 0))
                            (eol (line-end-position)))

                        ;; When `end' is the end of the line (except for white-space).
                        (goto-char end)
                        (skip-chars-forward "[:blank:]" eol)
                        (when (eq (point) eol)
                          (goto-char (line-beginning-position))
                          ;; Before:
                          ;; - Review: some text :task:`123`
                          ;; After:
                          ;; - Review: :pr:`123` some text
                          (when (looking-at "[[:blank:]-]+[[:alpha:]:]+\\([[:blank:]]+\\)" nil)
                            (let ((pr-text (buffer-substring-no-properties beg end)))
                              (while (eq ?\s (char-before beg))
                                (setq beg (1- beg)))
                              (delete-region beg end)
                              (goto-char (match-beginning 1))
                              (insert " :pr:" (substring pr-text 6)))))
                        (goto-char eol)))

                    (message "Replaced %d items" count))))))


;; Some additional highlights.
(with-eval-after-load 'hl-prog-extra
  (setq hl-prog-extra-list
        ;; Extend the default list.
        (append
         (list
          ;; Note that `nil' is used instead of comments as most text is not comments.
          '("\\<\\(DONE\\|OK\\)\\(([^)+]+)\\)?"
            ;; All text.
            0 nil '(:background "#006000" :foreground "#FFFFFF"))
          '("\\<\\(TODO\\|NOTE\\|WIP\\)\\(([^)+]+)\\)?"
            ;; All text.
            0 nil '(:background "#707000" :foreground "#FFFFFF"))
          '("\\<\\(FIXME\\|XXX\\|WARNING\\|BUG\\)\\(([^)+]+)\\)?"
            ;; All text.
            0 nil '(:background "#800000" :foreground "#FFFFFF"))

          '("\\<[TDP][[:digit:]]+\\>" 0 nil highlight-numbers-number)

          '("\\<\\(time:\\)\\([^\n]+\\)?\\>"
            ;; All text.
            0 nil font-lock-delimiter-face)
          (list
           (concat
            ;; Start of line.
            "^[[:blank:]]*"
            ;; Text.
            "\\<\\([[:alpha:]]+:\\)")
           ;; All text.
           0 nil 'font-lock-keyword-face))
         hl-prog-extra-list))
  (hl-prog-extra-refresh))

(with-eval-after-load 'cycle-at-point
  (setq-local cycle-at-point-list
              (append
               (list
                (list :data (list "TODO" "DONE") :case-fold nil)
                (list :data (list "FIXME" "FIXED") :case-fold nil))
               (cycle-at-point-preset))))


(defun rst-sha1-linkify-for-markdown ()
  "Convert SHA1 links to markdown links for DEVTALK."
  (interactive)

  (save-restriction
    (let ((table (make-syntax-table))
          (count 0))
      (with-syntax-table table
        (when (region-active-p)
          (narrow-to-region (region-beginning) (region-end)))

        ;; At least 6 characters for a SHA1.
        (goto-char (point-min))
        (let ((default-directory this-blender-dir))
          (while (re-search-forward "\\b\\([[:digit:]a-zA-F]+\\)\\b" nil t)
            (let ((git-subject
                   (string-trim-right
                    (shell-command-to-string
                     (concat
                      "git show --pretty=format:%s --no-patch " (match-string-no-properties 1))))))
              (replace-match (concat
                              "["
                              (regexp-quote git-subject)
                              "](https://projects.blender.org/blender/blender/commit/\\1)")
                             t nil nil)
              (setq count (1+ count)))))

        (message "Replaced %d items" count)))))

;; Local Variables:
;; fill-column: 99
;; indent-tabs-mode: nil
;; End:
