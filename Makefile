
SPHINXOPTS    =
PAPER         =
SPHINXBUILD   = sphinx-build
BUILDDIR      = build

BASE_DIR := ${CURDIR}

all: FORCE
	$(SPHINXBUILD) -j auto -b html $(SPHINXOPTS) ./content "$(BUILDDIR)/html"
	- echo " xdg-open \"$(BUILDDIR)/html/index.html\""

clean: FORCE
	rm -rf "$(BUILDDIR)/html" "$(BUILDDIR)/latex"

upload: all FORCE
	rsync --progress -ave "ssh -p 22" $(BUILDDIR)/html/* ideasman42@download.real.blender.org:/data/www/vhosts/download.blender.org/ftp/ideasman42/donelist

build_and_upload_all: all FORCE
	bash $(BASE_DIR)/bin/build_and_upload_all.sh

report: FORCE
	bash $(BASE_DIR)/bin/weekly_report.sh

FORCE:
