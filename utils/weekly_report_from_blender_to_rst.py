#!/usr/bin/env python3
"""
This script is used with the output of Blender's weekly report generator.

./tools/triage/weekly_report.py --username=ideasman42 --hash-length=0 > /tmp/out.txt
cat /tmp/out.txt | ./utils/weekly_report_from_blender_to_rst.py
"""
import io
import re
import sys

from typing import (
    Dict,
    List,
    NamedTuple,
    Tuple,
)

RE_MATCH_REF = "([a-zA-Z][a-zA-Z_-]*/[a-zA-Z][a-zA-Z0-9_-]*)([!#@])([a-z0-9]+)"


def md_ref_to_rst(repo_name: str, repo_ref_type: str, repo_ref_id: str) -> str:
    if repo_name == "blender/blender":
        repo_prefix = "B"
    elif repo_name == "blender/blender-manual":
        repo_prefix = "BM"
    elif repo_name == "blender/blender-developer-docs":
        repo_prefix = "BDM"
    else:
        repo_prefix = "<{:s}>" .format(repo_name)

    if repo_ref_type == "!":
        repo_link_type = "pr"
    elif repo_ref_type == "#":
        repo_link_type = "task"
    elif repo_ref_type == "@":
        repo_link_type = "rev"
    else:
        repo_link_type = "<?{:s}?>" .format(repo_ref_type)

    # No repository prefix pr/tasks.
    if repo_link_type in {"pr", "task"}:
        if repo_prefix == "B":
            repo_prefix = ""

    return ":{:s}:`{:s}{:s}`".format(repo_link_type, repo_prefix, repo_ref_id)


def md_ref_to_rst_in_content(data: str) -> str:
    def key_replace(match: re.Match[str]) -> str:
        a, b, c = match.groups()
        return md_ref_to_rst(a, b, c)

    data = re.sub(
        RE_MATCH_REF,
        key_replace, data
    )

    return data


class ActionReference(NamedTuple):
    contents: str
    repo_name: str
    repo_ref_type: str
    repo_ref_id: str

    def to_rst(action: "ActionReference") -> str:

        ref = md_ref_to_rst(action.repo_name, action.repo_ref_type, action.repo_ref_id)

        contents = md_ref_to_rst_in_content(action.contents)

        if ref.startswith(":rev:"):
            return (
                "- {:s}\n"
                "  {:s}"
            ).format(contents, ref)
        else:
            return "- {:s}: {:s}".format(ref, contents)


def parse_data(data: str) -> str:

    SectionValue = Tuple[List[str], List[ActionReference]]

    sections = ""
    section_map: Dict[str, SectionValue] = {}

    current_section: SectionValue = (
        # Regular lines.
        [],
        # Parsed fields such as commits.
        [],
    )
    section_map[""] = current_section

    re_comp_list_item = re.compile("(?:\\* +)([^\n]+) \\(" + RE_MATCH_REF + "\\)\\Z")
    for l in data.split("\n"):
        if l.startswith("#"):
            current_section[0].append(l)
            continue
        if l.strip() == "":
            continue

        if l.startswith("**") and l.endswith("**"):
            current_section = ([], [])
            section_key = l.strip("*")
            assert section_key not in section_map
            section_map[section_key] = current_section
            continue
        m = re_comp_list_item.match(l)
        if m:
            g = m.groups()
            current_section[1].append(ActionReference(*g))
        else:
            current_section[0].append(l)

    out = io.StringIO()
    for section_key, (literal_lines, action_lines) in section_map.items():
        if section_key:
            out.write("\n**{:s}**\n\n".format(section_key))

        # if literal_lines and action_lines:
        #     assert 0, "Both should not be set"

        if literal_lines and action_lines:
            out.write("\nXXX: unexpected both set, {:d}\n".format(len(action_lines)))

        if literal_lines:
            for f in literal_lines:
                if f.startswith("* "):
                    f = "-" + f[1:]
                out.write("{:s}\n".format(f))

        if action_lines:
            groups: Dict[str, List[ActionReference]] = {}
            for g in action_lines:
                groups[g[-3]] = []
            for g in action_lines:
                groups[g[-3]].append(g)

            for group, g_ls in groups.items():
                # Put cleanups at the end.
                for prefix in ("Fix :", "Fix ", "Extension", "Cleanup:"):
                    g_ls_prefix = []
                    for i in reversed(range(len(g_ls))):
                        if g_ls[i].contents.startswith(prefix):
                            g_ls_prefix.append(g_ls.pop(i))
                    if g_ls_prefix:
                        g_ls_prefix.reverse()
                        g_ls.extend(g_ls_prefix)
                    del g_ls_prefix

                for g in g_ls:
                    out.write("{:s}\n".format(ActionReference.to_rst(g)))
                out.write("\n")
        if literal_lines and action_lines:
            out.write("\nXXX: unexpected both set -- end\n")

    return out.getvalue()


def main() -> None:
    sys.stdout.write(parse_data(sys.stdin.read()))


if __name__ == "__main__":
    main()
