#!/usr/bin/env python
import os
import sys

import rst_to_md.cli

CWD = os.path.abspath(os.path.dirname(__file__))


def main():
    argv_orig = sys.argv

    content_src_dir = os.path.join(CWD, "content")
    content_dst_dir = os.path.join(CWD, "content_md")
    paths = [f for f in os.listdir(content_src_dir) if f.endswith(".rst")]
    paths.remove("index.rst")
    paths.sort()
    del paths[:-1]  # Only 2024.
    for f in paths:
        filename_src = os.path.join(content_src_dir, f)
        filename_dst = os.path.join(content_dst_dir, f.replace(".rst", ".md"))
        sys.argv = argv_orig + [filename_src, "--output=" + filename_dst]
        rst_to_md.cli.main()

    sys.argv = argv_orig


if __name__ == "__main__":
    main()
