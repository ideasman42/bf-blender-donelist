
_USE_CUSTOM_THEME = True

import sys
import os

sys.path.insert(0, os.path.abspath(os.path.join('..', 'exts')))

extensions = [
    # developer.blender.org
    'extlinks_plus',
    'bl_rev',

    # general
    'youtube',
]


# Similar to extlinks but for version control, options for shorten SHA1's
extlinks_plus = {
    # Match `gitea` syntax. Slug-prefix-mapping.
    'task': (
        'https://projects.blender.org/blender/%s/issues/%s', '%s: (%s)', '#%s',
        {
            'BA': 'blender-addons',
            'BM': 'blender-manual',
            'BDM': 'blender-developer-docs',
            '': 'blender',
        },
    ),

    'pr': (
        'https://projects.blender.org/blender/%s/pulls/%s', '%s: (%s)', '!%s',
        {
            'BAC': 'blender-addons-contrib',
            'BA': 'blender-addons',
            'BM': 'blender-manual',
            'BDM': 'blender-developer-docs',
            '': 'blender',
        }
    ),

    # Not migrated, need another way to reference PR? (!number).
    'diff': ('http://developer.blender.org/D%s', '%s: (%s)', 'D%s', None),

    'paste': ('http://developer.blender.org/P%s', '%s: (%s)', 'P%s', None),

    # Use bl_rev since it can validate sha1's.
    # Show 9 chars of revision (typically there is a B prefix, so 8 chars or SHA1).
    # 'rev': ('http://developer.blender.org/r%s', '%s: (%s)', 'r%.9s'),
}

# The suffix of source filenames.
source_suffix = '.rst'

exclude_patterns = ['template.rst']
master_doc = 'index'

# General information about the project.
project = 'Weekly Done-List of ideasman42'
copyright = 'Creative Commons'

# without this it calls it 'documentation', which it's not
html_title = 'weekly-donelist-ideasman42'
html_short_title = 'ideasman42\'s weekly done-list'

# visual noise for my purpose
html_show_copyright = False
html_show_sphinx = False
html_show_sourcelink = False

'''
try:
    import sphinx_rtd_theme
except ImportError:
    sphinx_rtd_theme = None

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
if sphinx_rtd_theme:
    html_theme = 'sphinx_rtd_theme'
else:
    html_theme = 'haiku'

if sphinx_rtd_theme:
    html_theme_path = [sphinx_rtd_theme.get_html_theme_path()]
'''

html_theme = 'alabaster'
html_theme_options = {
    "show_powered_by": False,
}
import alabaster
print(alabaster.__file__)
html_theme_path = [alabaster.get_path()]

# for our own extensions! (bl_rev)
phabricator_base = 'http://developer.blender.org'

bl_rev_site_base = 'https://'
bl_rev_site_prefix_map = {
    'BAC': "projects.blender.org/blender/blender-addons-contrib/commit/",
    'BA': "projects.blender.org/blender/blender-addons/commit/",
    'BM': "projects.blender.org/blender/blender-manual/commit/",
    'BDT': "projects.blender.org/blender/blender-dev-tools/commit/",
    'BL': "projects.blender.org/?/",
    'B': "projects.blender.org/blender/blender/commit/",
    # Application templates.
    'AT': "projects.blender.org/?/",
}

if _USE_CUSTOM_THEME:
    html_theme = "classic"

    # InkPot theme: https://bitbucket.org/birkenfeld/pygments-main/pull-requests/751
    # BEGIN INKPOT THEME
    from pygments.style import Style
    from pygments.token import \
        Text, Other, \
        Keyword, Name, Comment, String, Error, \
        Number, Operator, Generic, Whitespace, Punctuation

    class InkPotStyle(Style):
        background_color = "#1e1e27"
        default_style = ""
        styles = {
            Text: "#cfbfad",
            Other: "#cfbfad",
            Whitespace: "#434357",
            Comment: "#cd8b00",
            Comment.Preproc: "#409090",
            Comment.PreprocFile: "bg:#404040 #ffcd8b",
            Comment.Special: "#808bed",

            Keyword: "#808bed",
            Keyword.Pseudo: "nobold",
            Keyword.Type: "#ff8bff",

            Operator: "#666666",

            Punctuation: "#cfbfad",

            Name: "#cfbfad",
            Name.Attribute: "#cfbfad",
            Name.Builtin.Pseudo: '#ffff00',
            Name.Builtin: "#808bed",
            Name.Class: "#ff8bff",
            Name.Constant: "#409090",
            Name.Decorator: "#409090",
            Name.Exception: "#ff0000",
            Name.Function: "#c080d0",
            Name.Label: "#808bed",
            Name.Namespace: "#ff0000",
            Name.Variable: "#cfbfad",

            String: "bg:#404040 #ffcd8b",
            String.Doc: "#808bed",

            Number: "#f0ad6d",

            # from emacs
            Generic.Heading: "bold #000080",
            Generic.Subheading: "bold #800080",
            Generic.Deleted: "#A00000",
            Generic.Inserted: "#00A000",
            Generic.Error: "#FF0000",
            Generic.Emph: "italic",
            Generic.Strong: "bold",
            Generic.Prompt: "bold #000080",
            Generic.Output: "#888",
            Generic.Traceback: "#04D",

            Error: "bg:#6e2e2e #ffffff"
        }

    def pygments_monkeypatch_style(mod_name, cls):
        import sys
        import pygments.styles
        cls_name = cls.__name__
        mod = type(__import__("os"))(mod_name)
        setattr(mod, cls_name, cls)
        setattr(pygments.styles, mod_name, mod)
        sys.modules["pygments.styles." + mod_name] = mod
        from pygments.styles import STYLE_MAP
        STYLE_MAP[mod_name] = mod_name + "::" + cls_name

    pygments_monkeypatch_style("inkpot", InkPotStyle)
    pygments_style = "inkpot"
    # END INKPOT THEME

    html_theme_options = {
        "bgcolor": "#ffffff",
        "textcolor": "#000000",
        "sidebarbgcolor": "#343131",
        "sidebartextcolor": "#ffffff",
        "sidebarlinkcolor": "#a7b3b3",
        "linkcolor": "#715ab1",
        "headlinkcolor": "#715ab1",
        "headbgcolor": "#fbf8ef",
        "headtextcolor": "#715ab1",
        "codebgcolor": InkPotStyle.background_color,
        "codetextcolor": "black",
        "relbarbgcolor": "#715ab1",
        "relbartextcolor": "#fff7be",
        "relbarlinkcolor": "#fff7be",
        "footerbgcolor": "#343131",
        "footertextcolor": "#ffffff"
    }
